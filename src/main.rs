extern crate crc32;

use std::io::{stdin, Read, Result};
use std::ffi::OsString;

fn main() {
    let files = std::env::args_os().skip(1);
    if files.len() == 0 {
        // Calculate crc from stdin
        let stdin = stdin();
        let locked = stdin.lock();
        let crc32_result = crc32::crc32_result(locked);
        print_result(crc32_result);
    } else {
        for file in files {
            print_file_crc32(file);
        }
    }
}

fn print_file_crc32(file: OsString) {
    let filehandle_result = std::fs::File::open(file);
    match filehandle_result {
        Ok(v) => print_crc32(v),
        Err(e) => panic!("Error: {:?}", e),
    }
}

fn print_crc32<T: Read>(data: T) {
    let crc32_result = crc32::crc32_result(data);
    print_result(crc32_result);
}

fn print_result(result: Result<u32>) {
    match result {
        Ok(v) => println!("{:08X}", v),
        Err(e) => panic!("Error: {:?}", e),
    }
}